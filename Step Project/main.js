let tab = function () {
  let tabClick = document.querySelectorAll('.our-services-direction'),
    pictures = document.querySelectorAll('.our-services-img'),
    tabText = document.querySelectorAll('.under-our-services-switching-text'),
    tabName;
  tabClick.forEach(item => {
    item.addEventListener('click', selectTab);
  });
  function selectTab() {
    document.querySelector('.is-active').classList.remove('is-active');
    this.classList.add('is-active');
    tabName = this.getAttribute('data-tab-name');
    selectTabContent(tabName);
  }
  function selectTabContent(tabName) {
    pictures.forEach(item => {
      item.classList.contains(tabName) ? item.classList.add('is-active') : item.classList.remove('is-active');
    });
    tabText.forEach(item => {
      item.classList.contains(tabName) ? item.classList.add('is-active') : item.classList.remove('is-active');
    })
  }
};
tab();
let button = document.querySelector('.load-more-button');
let none = document.querySelector('.none');
button.addEventListener('click', () => {
  none.style.display = 'block';
  button.remove();
});
let tabs = function () {
  let tabClick2 = document.querySelectorAll('.our-amazing-work-switching-text'),
    img_block = document.querySelectorAll('.our-amazing-work-img'),
    tabName;
  tabClick2.forEach(item => {
    item.addEventListener('click', selectTab);
  });
  function selectTab() {
    document.querySelector('.active_tab').classList.remove('active_tab');
    this.classList.add('active_tab');
    tabName = this.getAttribute('data-tab');
    selectTabContent(tabName)
  }
  function selectTabContent() {
    img_block.forEach(item => {
      item.classList.contains(tabName) ? item.style.display = "block" : item.style.display = "none";
    });
  }
};
tabs();

$(document).ready(function () {
  $('.slider-icon').slick({
    arrows: true,
    dots: false,
    waitForAnimate: false,
    slidesToScroll: 1,
    slidesToShow: 4,
    focusOnSelect: true,
    asNavFor: ".slider",
    draggable: false
  });
  $('.slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    adaptiveHeight: true,
    asNavFor: ".slider-icon"
  })

});
