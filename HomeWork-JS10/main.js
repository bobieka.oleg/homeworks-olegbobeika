let item = document.querySelector('#item'),
    item2 = document.querySelector('#item2'),
    password = document.querySelector(".password"),
    password2 = document.querySelector(".password2"),
    button = document.querySelector('.btn'),
    label = document.querySelector('#text'),
    p = document.createElement('p');
p.innerHTML = 'Нужно ввести одинаковые значения';
p.style.color = 'red';

function openEye(item) {
    item.classList.contains('fa-eye') ? item.classList.replace('fa-eye', 'fa-eye-slash') : item.classList.replace('fa-eye-slash', 'fa-eye');
    if (password.getAttribute('type') === 'password') {
        password.setAttribute('type', 'text');

    } else {
        password.setAttribute('type', 'password');

    }
}

item.addEventListener('click', () => openEye(item));

function openEye2(item2) {
    item2.classList.contains('fa-eye') ? item2.classList.replace('fa-eye', 'fa-eye-slash') : item2.classList.replace('fa-eye-slash', 'fa-eye');
    if (password2.getAttribute('type') === 'password') {
        password2.setAttribute('type', 'text');
    } else {
        password2.setAttribute('type', 'password');
    }
}

item2.addEventListener('click', () => openEye2(item2));
button.addEventListener('click', function (button) {
    button.preventDefault();
    if (password.value === password2.value) {
        alert('You are welcome')
        p.remove()
    } else {
        label.append(p);
    }
});
